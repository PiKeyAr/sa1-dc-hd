# Sonic Adventure DC-HD Image Builder

**This program has been superseded by [Dreamcast Image Builder](https://gitlab.com/PiKeyAr/dreamcast-image-builder/) and is no longer in development.**

This repository hosts a program that applies a set of patches (mods) to the Dreamcast version of Sonic Adventure to introduce various fixes and improvements.

The modded version is meant to be run on the [flycast emulator](https://flyinghead.github.io/flycast-builds), although *some* (not all!) mods will also work on other emulators and original Dreamcast hardware.

The user interface of this tool is inspired by and built to resemble the classic Sonic Adventure DX Mod Manager by MainMemory.

Autodemo support is now included in the image builder, which lets you apply mods to the Autodemo prototype as well (there are no built-in mods for the Autodemo at the moment).

**LIST OF BUILT-IN MODS (FOR THE FINAL VERSION)**

- The game runs at 60 FPS during gameplay without framerate-related glitches.
- Widescreen hack without model clipping or HUD stretching.
- All FMVs can be skipped by pressing Start.
- Cutscenes can be skipped by holding B before a cutscene loads.
- If you cannot run in a straight line with your controller/keyboard, there is a patch to fix that.
- Minor bugfixes for some levels such as object placement, Z fighting fixes etc.
- Better level and object draw distance.
- Some mods made for the PC version of Sonic Adventure DX may have Dreamcast adaptations in the future.

**LIST OF CHEAT CODES (FOR THE FINAL VERSION)**
- All characters unlocked in Adventure Mode.
- Easy fishing (infinite rod tension).


All mods are optional and can be toggled individually before building the image. Cheat codes can be toggled in flycast's Cheats menu.


**PREREQUISITES**

- [.NET 7.0 runtime](https://dotnet.microsoft.com/en-us/download/dotnet/7.0). **Get the DESKTOP version.**
- Sonic Adventure US 1.005 (or Sonic Adventure Autodemo Prototype if you want to mod that) GDI image
- [flycast emulator](https://flyinghead.github.io/flycast-builds/).
- Dreamcast BIOS for flycast (optional?).

**As of May 2024, only flycast is compatible with the 60 FPS code and the current implementation of the widescreen hack. The cheat table is in the RetroArch .cht format used by flycast.**

**HOW TO USE**

See [this wiki page](https://gitlab.com/PiKeyAr/sa1-dc-hd/-/wikis/Using-Sonic-Adventure-Image-Builder).

**CONFIGURING FLYCAST**

See [this wiki page](https://gitlab.com/PiKeyAr/sa1-dc-hd/-/wikis/Configuring-flycast-for-the-modded-image).

**CREDITS**

- [Exant](https://github.com/Exant64) for work on the hardest parts related to widescreen and skippable cutscenes, as well as general help
- woofmute and Speeps for Autodemo mods

This program relies on the following tools and libraries:
- [FamilyGuy's gditools](https://sourceforge.net/projects/dcisotools/)
- [DiscUtils](https://github.com/DiscUtils/DiscUtils) fork from [Sappharad's GDIbuilder](https://github.com/Sappharad/GDIbuilder)
- [Sewer56's dlang-prs](https://github.com/Sewer56/dlang-prs)
- [ini-parser](https://github.com/rickyah/ini-parser)
- [SonicFreak94's pvmx](https://github.com/michael-fadely/pvmx)
- SonicFreak94's dcdimake tool
- mkisofs
- cdi4dc
