﻿using IniParser.Model;
using System;
using System.Globalization;
using System.IO;

namespace GUIPatcher
{
    public partial class MainForm
    {
        private bool InstallMod(string datapath, Mod data, bool autodemo)
        {
            string replFolder = (Path.Combine(datapath, "SONICADV"));
            if (autodemo)
                replFolder = datapath;

            if (data.FileReplacements != null && data.FileReplacements.Length > 0)
            {
                LogMessage("Processing added/modified files:");
                foreach (string file in data.FileReplacements)
                {
                    string replFilePath = Path.Combine(replFolder, Path.GetFileName(file));
                    File.Copy(file, replFilePath, true);
                    LogMessage(string.Format("{0}", Path.GetFileName(file)));
                    // If the replacement file is a PRS that is going to be patched, decompress it
                    if (Path.GetExtension(file).ToLowerInvariant() == ".prs")
                    {
                        if (PRSFiles.Contains(Path.GetFileName(file).ToUpperInvariant()))
                        {
                            LogMessage(string.Format("Decompressing a replaced PRS file: {0}", Path.GetFileName(file)));
                            byte[] prsFileData = csharp_prs.Prs.Decompress(File.ReadAllBytes(replFilePath));
                            File.WriteAllBytes(Path.ChangeExtension(replFilePath, ".bin"), prsFileData);
                        }
                    }
                }
            }
            if (data.IncludeFolders != null && data.IncludeFolders.Length > 0)
            {
                LogMessage("Processing included directories:");
                foreach (string folder in data.IncludeFolders)
                {
                    LogMessage(string.Format("Folder: {0}", Path.Combine(data.ModPath, folder)));
                    string[] files = Directory.GetFiles(Path.Combine(data.ModPath, folder), "*.*", SearchOption.AllDirectories);
                    if (files != null && files.Length > 0)
                    {
                        foreach (var f in files)
                        {
                            LogMessage(string.Format("File: {0}", Path.GetFileName(f)));
                            string replFilePath = Path.Combine(replFolder, Path.GetFileName(f));
                            File.Copy(f, replFilePath, true);
                        }
                    }
                    else
                        LogMessage("Error processing included files");
                }
            }
            foreach (SectionData section in data.PatchData.Sections)
            {
                string destFilePath = Path.Combine(datapath, section.SectionName);
                if (!File.Exists(destFilePath))
                {
                    LogMessage(string.Format("File {0} doesn't exist", destFilePath));
                    return false;
                }
                bool isPRS = Path.GetExtension(section.SectionName.ToLowerInvariant()) == ".prs";
                LogMessage(string.Format("Processing patch data for {0}", section.SectionName));
                byte[] fileData = File.ReadAllBytes(isPRS ? Path.ChangeExtension(destFilePath, ".bin") : destFilePath);
                foreach (KeyData key in section.Keys)
                {
                    int address = int.Parse(key.KeyName, System.Globalization.NumberStyles.HexNumber);
                    string value = key.Value.Replace(" ", "");
                    LogMessage(string.Format("{0}:{1}", address.ToString("X"), value), false);
                    if (value.Length % 2 != 0)
                    {
                        LogMessage("Incorrect length of the hex string - must be divisible by 2");
                        return false;
                    }
                    // Resize data if the patch adds to the file
                    if (address + value.Length / 2 >= fileData.Length)
                    {
                        int newsize = address + value.Length / 2;
                        if (newsize % 4 != 0)
                        {
                            do
                                newsize++;
                            while (newsize % 4 != 0);
                        }
                        Array.Resize(ref fileData, newsize);
                        LogMessage(string.Format("Resized binary to {0} (0x{1})", fileData.Length, fileData.Length.ToString("X")));
                    }
                    int cnt = 0;
                    for (int i = 0; i < value.Length; i += 2)
                    {
                        //byte toWrite = byte.Parse(value.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
                        //#if DEBUG
                        //LogMessage(string.Format("Changing {0} from {1} to {2}", (address + cnt).ToString("X"), fileData[address + i].ToString("X"), toWrite.ToString("X"));
                        //#endif
                        fileData[address + cnt] = byte.Parse(value.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
                        cnt++;
                    }
                } 
                if (!Directory.Exists(Path.GetDirectoryName(destFilePath)))
                    Directory.CreateDirectory(Path.GetDirectoryName(destFilePath));
                File.WriteAllBytes(isPRS ? Path.ChangeExtension(destFilePath, ".bin") : destFilePath, fileData);
            }
            if (data.PatternFiles != null && data.PatternFiles.Length > 0)
            {
                LogMessage(string.Format("{0}: Applying pattern patches", data.Name));
                foreach (string patternfile in data.PatternFiles)
                {
                    LogMessage(patternfile);
                    string destFilePath2 = Path.Combine(datapath, patternfile);
                    if (!File.Exists(destFilePath2))
                    {
                        LogMessage(string.Format("File '{0}' not found for pattern matching", destFilePath2));
                        continue;
                    }
                    bool isPRS = Path.GetExtension(destFilePath2.ToLowerInvariant()) == ".prs";
                    byte[] fileData = File.ReadAllBytes(isPRS ? Path.ChangeExtension(destFilePath2, ".bin") : destFilePath2);
                    string patini = Path.Combine(data.ModPath, Path.ChangeExtension(patternfile, ".ini"));
                    string[] patternData = File.Exists(patini) ? File.ReadAllLines(patini) : data.PatternData;
                    foreach (string pattern in patternData)
                    {
                        string[] pat = pattern.Split(',');
                        // Ignore commented out patches
                        if (pat[0].StartsWith(';'))
                            continue;
                        if (pat.Length < 2)
                        {
                            LogMessage(string.Format("Incorrect syntax for pattern '{0}'", pattern));
                            continue;
                        }
                        bool res = uint.TryParse(pat[1], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out uint addr_src);
                        if (!res)
                        {
                            LogMessage(string.Format("Incorrect syntax for pattern '{0}'", pattern));
                            continue;
                        }
                        res = uint.TryParse(pat[2], NumberStyles.HexNumber, CultureInfo.InvariantCulture, out uint addr_dst);
                        if (!res)
                        {
                            LogMessage(string.Format("Incorrect syntax for pattern '{0}'", pattern));
                            continue;
                        }
                        if (fileData.Length == 0)
                        {
                            LogMessage(string.Format("File size is 0: '{0}'", isPRS ? Path.ChangeExtension(destFilePath2, ".bin") : destFilePath2));
                            continue;
                        }
                        if (addr_src != 0 && addr_dst != 0)
                        {
                            for (int addr = 0; addr < fileData.Length - 4; addr += 4)
                            {
                                //LogMessage(string.Format("{0}: checking value {1} at address {2}", isPRS ? Path.ChangeExtension(destFilePath2, ".bin") : destFilePath2, addr_src, addr);
                                if (BitConverter.ToUInt32(fileData, addr) == addr_src)
                                {
                                    //LogMessage(string.Format("{0}: copying value {1} to address {2} in", isPRS ? Path.ChangeExtension(destFilePath2, ".bin") : destFilePath2, addr_dst, addr);
                                    BitConverter.GetBytes(addr_dst).CopyTo(fileData, addr);
                                }
                            }
                        }
                    }
                    File.WriteAllBytes(isPRS ? Path.ChangeExtension(destFilePath2, ".bin") : destFilePath2, fileData);
                }
            } 
            LogMessage(string.Format("Mod {0} applied.", data.Name));
            LogMessage("");
            return true;
        }
    }
}