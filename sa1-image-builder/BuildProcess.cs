﻿using DiscUtils.Gdrom;
using IniParser;
using IniParser.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace GUIPatcher
{
    public partial class MainForm
    {
        // Step 1 - Find the GDI file and extract track03 or reuse original data from the orig_data folder
        async void BuildStart(CancellationToken ct)
        {
            building = true;
            string workdir_main = textBoxOutputPath.Text;
            string workdir_data = Path.Combine(textBoxOutputPath.Text, "data");
            string workdir_data_orig = Path.Combine(textBoxOutputPath.Text, "data_orig");

            string filename; // Process filename
            string args; // Process arguments

            // Logger
            string logFile = Path.Combine(workdir_main, "BuildLog.txt");

            // Delete log if it exists
            if (File.Exists(logFile))
            {
                try 
                { 
                    File.Delete(logFile); 
                } 
                catch 
                { 
                    Console.WriteLine("Unable to delete log file.");
                    logger = null;
                }
            }

            // Create log
            try
            {
                logger = File.CreateText(logFile);
            }
            catch
            {
                Console.WriteLine("Unable to open log file.");
                logger = null;
            }

            // Update UI
            RefreshProgress("Step 1/6: Extracting original data");

            // Create output folder
            Directory.CreateDirectory(textBoxOutputPath.Text);

            // Clean working folder
            if (Directory.Exists(workdir_data))
            {
                try
                {
                    Directory.Delete(workdir_data, true);
                }
                catch
                {
                    LogMessage(string.Format("Unable to clean up data folder {0}", workdir_data));
                }

            }

            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("1");
                BuildFinish();
                return;
            }

            // Reuse data_orig folder if it exists
            if (Directory.Exists(workdir_data_orig))
            {
                LogMessage("Original data folder found, copying files...");
                // Copy files to the working folder
                CopyDirectory(workdir_data_orig, workdir_data, true, ct);
            }
            // Otherwise extract the GDI
            else
            {
                string[] gdis = Directory.GetFiles(textBoxOriginalPath.Text, "*.gdi");
                if (gdis.Length == 0)
                {
                    LogMessage("GDI file not found! Aborting.");
                    BuildFinish();
                    return;
                }
                else if (gdis.Length > 1)
                {
                    LogMessage("Multiple GDI files found. Please use only one GDI file. Aborting.");
                    BuildFinish();
                    return;
                }
                filename = "gditools.exe";
                args = "-i \"" + gdis[0] + "\"" + " -o \"" + workdir_main + "\"" + " --extract-all -b ip.bin";
                try
                {
                    var exitCode = await ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", filename), args, cancellationTokenSource, textBoxOutputPath.Text, null, Console.Out, Console.Out);
                    LogMessage(string.Format("Process {0} exited with Exit Code {1}", filename, exitCode));
                }
                catch (TaskCanceledException)
                {
                    LogMessage(string.Format("Process {0} killed", filename));
                }
            }

            // Go to the next step
            ApplyMods(ct);
        }

        // Step 2 - Apply mods
        void ApplyMods(CancellationToken ct)
        {
            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("2");
                BuildFinish();
                return;
            }

            // Set working folder
            string workdir_data = Path.Combine(textBoxOutputPath.Text, "data");
            string workdir_data_orig = Path.Combine(textBoxOutputPath.Text, "data_orig");

            // If the orig data folder doesn't exist, create it and copy files there for next use
            if (!Directory.Exists(workdir_data_orig))
            {
                // Copy files to the original data folder to reduce build time next time
                LogMessage("Creating original data folder");
                CopyDirectory(workdir_data, workdir_data_orig, true, ct);
            }

            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("3");
                BuildFinish();
                return;
            }

            // Check if data extracted correctly (1ST_READ exists)
            if (!File.Exists(Path.Combine(workdir_data, "1ST_READ.BIN")))
            {
                LogMessage(string.Format("Error: File {0} not found", Path.Combine(workdir_data, "1ST_READ.BIN")));
                return;
            }

            // Delete track03.iso (leftover from gdi2data)
            if (File.Exists(Path.Combine(textBoxOutputPath.Text, "track03.iso")))
                File.Delete(Path.Combine(textBoxOutputPath.Text, "track03.iso"));

            // Update UI
            RefreshProgress("Step 2/6: Patching files for mods");

            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("4");
                BuildFinish();
                return;
            }

            // Select active mods to apply
            List<Mod> applyMods = new List<Mod>();
            for (int u = 0; u < activeMods.Count; u++)
            {
                for (int i = 0; i < mods.Count; i++)
                {
                    if (mods[i].Name == activeMods[u])
                    {
                        LogMessage(string.Format("Mod: {0}", mods[i].Name));
                        applyMods.Add(mods[i]);
                        break;
                    }

                }
            }

            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("5");
                BuildFinish();
                return;
            }

            // Make a list of PRS files that will be patched
            foreach (Mod mod in applyMods)
            {
                // Make a list of PRS files changed by mods
                foreach (SectionData section in mod.PatchData.Sections)
                {
                    if (Path.GetExtension(section.SectionName.ToLowerInvariant()) == ".prs")
                        if (!PRSFiles.Contains(section.SectionName.ToUpperInvariant()))
                            PRSFiles.Add(section.SectionName.ToUpperInvariant());
                }
                if (mod.PatternFiles != null && mod.PatternFiles.Length > 0)
                {
                    foreach (string patternfile in mod.PatternFiles)
                    {
                        if (Path.GetExtension(patternfile.ToLowerInvariant()) == ".prs")
                            if (!PRSFiles.Contains(patternfile.ToUpperInvariant()))
                                PRSFiles.Add(patternfile.ToUpperInvariant());
                    }
                }
            }

            // Extract all PRS files in the list
            if (PRSFiles.Count > 0)
            {
                LogMessage(string.Format("Extracting {0} PRS files...", PRSFiles.Count));
                foreach (string prsFile in PRSFiles)
                {
                    LogMessage(prsFile, false);
                    string prsFullPath = Path.Combine(workdir_data, prsFile);
                    byte[] prsFileData = csharp_prs.Prs.Decompress(File.ReadAllBytes(prsFullPath));
                    File.WriteAllBytes(Path.ChangeExtension(prsFullPath, ".bin"), prsFileData);
                    if (ct.IsCancellationRequested)
                    {
                        //Console.WriteLine("6");
                        BuildFinish();
                        return;
                    }
                }
            }
            LogMessage(string.Format("\nApplying mods...\n"));
            // Apply mods
            bool errors = false;
            foreach (Mod mod in applyMods)
            {
                if (ct.IsCancellationRequested)
                {
                    //Console.WriteLine("7");
                    BuildFinish();
                    return;
                }
                LogMessage(string.Format("Mod: {0}", mod.Name));
                bool result = InstallMod(workdir_data, mod, autodemoMode);
                if (!result)
                {
                    errors = true;
                    LogMessage(string.Format("Error installing mod {0}", mod.Name));
                }
            }
            if (errors)
            {
                LogMessage("Error applying mods. Check the log and try again.");
                return;
            }

            // Recompress PRS files
            if (PRSFiles.Count > 0)
                LogMessage("Compressing PRS files...");
            foreach (string prsFile in PRSFiles)
            {
                if (ct.IsCancellationRequested)
                {
                    //Console.WriteLine("8");
                    BuildFinish();
                    return;
                }
                LogMessage(prsFile, false);
                string prsFullPath = Path.Combine(workdir_data, prsFile);
                byte[] binFileData = File.ReadAllBytes(Path.ChangeExtension(prsFullPath, ".bin"));
                byte[] compressedData = csharp_prs.Prs.Compress(ref binFileData, 255);
                File.WriteAllBytes(prsFullPath, compressedData);
                File.Delete(Path.ChangeExtension(prsFullPath, ".bin"));
            }

            // Proceed to the next step
            CopyOriginalDataTracks(ct);
        }

        // Step 3 - Copy back original data tracks
        private void CopyOriginalDataTracks(CancellationToken ct)
        {
            RefreshProgress("Step 3/6: Copying original data tracks");
            if (ct.IsCancellationRequested)
            {
                //Console.WriteLine("9");
                BuildFinish();
                return;
            }
            if (!checkBoxCDI.Checked)
            {
                string[] originalFiles = Directory.GetFiles(textBoxOriginalPath.Text, "*.*", SearchOption.TopDirectoryOnly);
                for (int u = 0; u < originalFiles.Length; u++)
                {
                    if (Path.GetFileNameWithoutExtension(originalFiles[u]).ToLowerInvariant() != "track03")
                        switch (Path.GetExtension(originalFiles[u].ToLowerInvariant()))
                        {
                            case ".gdi":
                                string destpathg = Path.Combine(textBoxOutputPath.Text,"disc.gdi");
                                if (File.Exists(destpathg))
                                    LogMessage(string.Format("File already exists: {0}", Path.GetFileName(originalFiles[u])));
                                else
                                {
                                    LogMessage(string.Format("Copying file: {0}", Path.GetFileName(originalFiles[u])));
                                    File.Copy(originalFiles[u], destpathg, false);
                                }
                                break;
                            case ".bin":
                            case ".raw":
                            case ".iso":
                                string destpath = Path.Combine(textBoxOutputPath.Text, Path.GetFileName(originalFiles[u]));
                                if (File.Exists(destpath))
                                    LogMessage(string.Format("File already exists: {0}", Path.GetFileName(originalFiles[u])));
                                else
                                {
                                    LogMessage(string.Format("Copying file: {0}", Path.GetFileName(originalFiles[u])));
                                    File.Copy(originalFiles[u], destpath, false);
                                }
                                break;
                            default:
                                LogMessage(string.Format("File not needed: {0}", Path.GetFileName(originalFiles[u])));
                                break;
                        }
                }
            }
            else
                LogMessage("Not required for CDI");
            BuildImage(ct);
        }

        // Step 4 - Build track03.bin or CDI
        private async void BuildImage(CancellationToken ct)
        {
            // Set filenames
            string workdir = Path.Combine(textBoxOutputPath.Text);
            string dataDir = Path.Combine(workdir, "data");
            string ipBin = Path.Combine(Path.GetFullPath(workdir), "ip.bin");
            string ipBinCDI = Path.Combine(currentDirAss, "utils", autodemoMode ? "IP_CDI_AD.BIN" : "IP_CDI.BIN");
            string gdiOutput = workdir;

            // Update UI
            RefreshProgress("Step 4/6: Building a modified image");

            // Stop emulator if running
            TerminateEmu();

            // Build CDI
            if (checkBoxCDI.Checked)
            {
                LogMessage("Mode: CDI");

                if (ct.IsCancellationRequested)
                {
                    //Console.WriteLine("10");
                    BuildFinish();
                    return;
                }

                // Patch 1ST_READ for CDI
                byte[] fstread = File.ReadAllBytes(Path.Combine(dataDir, "1ST_READ.BIN"));
                if (autodemoMode)
                {
                    fstread[0x61F928] = 0xA6;
                    fstread[0x61F929] = 0x00;
                }
                else
                {
                    fstread[0x65C530] = 0xA6;
                    fstread[0x65C531] = 0x00;
                }
                File.WriteAllBytes(Path.Combine(dataDir, "1ST_READ.BIN"), fstread);

                // Copy hacked IP.BIN for CDI
                File.Copy(ipBinCDI, Path.Combine(dataDir, "IP.BIN"), true);

                // Build CDI
                string pfilename = "dcdimake.exe";
                string pargs = "-i " + "\"" + dataDir + "\"" + " -o " + "\"" + Path.Combine(workdir, "disc.cdi") + "\"";
                try
                {
                    var exitCode = await ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", pfilename), pargs, cancellationTokenSource, Path.Combine(currentDirAss, "utils"), null, Console.Out, Console.Out);
                    LogMessage(string.Format("Process {0} exited with Exit Code {1}", pfilename, exitCode));
                }
                catch (TaskCanceledException)
                {
                    LogMessage(string.Format("Process {0} killed", pfilename));
                }
            }
            // Build GDI
            else
            {
                LogMessage(string.Format("Mode: GDI"));

                if (ct.IsCancellationRequested)
                {
                    //Console.WriteLine("11");
                    BuildFinish();
                    return;
                }

                // Check if IP.BIN exists
                if (!File.Exists(Path.Combine(workdir, "IP.BIN")))
                {
                    LogMessage("Extracting IP.BIN from the GDI file...");
                    // If it doesn't, extract it from the GDI
                    // Find GDIs
                    string[] gdis = Directory.GetFiles(textBoxOriginalPath.Text, "*.gdi");
                    if (gdis.Length == 0)
                    {
                        LogMessage("GDI file not found! Aborting.");
                        BuildFinish();
                        return;
                    }
                    else if (gdis.Length > 1)
                    {
                        LogMessage("Multiple GDI files found. Please use only one GDI file. Aborting.");
                        BuildFinish();
                        return;
                    }
                    string filename = "gditools.exe";
                    string args = "-i \"" + gdis[0] + "\"" + " -o \"" + workdir + "\"" + " -b ip.bin";
                    try
                    {
                        var exitCode = await ProcessStuff.StartProcess(Path.Combine(currentDirAss, "utils", filename), args, cancellationTokenSource, textBoxOutputPath.Text, null, Console.Out, Console.Out);
                        LogMessage(string.Format("Process {0} exited with Exit Code {1}", filename, exitCode));
                    }
                    catch (TaskCanceledException)
                    {
                        LogMessage(string.Format("Process {0} killed", filename));
                    }
                }
                GDromBuilder builder = new GDromBuilder();
                builder.ReportProgress += GDIProgressReport;
                builder.RawMode = true;
                builder.TruncateData = false;
                List<DiscTrack> tracks = null;
                tracks = builder.BuildGDROM(dataDir, ipBin, null, gdiOutput);
                LogMessage(string.Format("Track details: " + builder.GetGDIText(tracks)));
            }
            //Console.WriteLine("12");
            BuildFinish();
        }

        private static void GDIProgressReport(int amount)
        {
            if (amount % 10 == 0)
            {
                Console.WriteLine(amount.ToString() + "%");
            }
        }

        private void BuildFinish()
        {
            string codesOutPath = Path.Combine(textBoxOutputPath.Text, "cheats.cht");
            // Terminate processes if the build was cancelled
            if (buildCancel)
            {
                string[] processes = { "gditools", "dcdimake", "mkisofs", "cdi4dc" };
                foreach (string process in processes)
                    TerminateProcess(process);
            }
            else
            {
                // Step 5 - apply codes
                // Apply codes
                if (codes != null)
                {
                    RefreshProgress("Step 5/6: Applying codes");
                    for (int c = 0; c < 99; c++)
                    {
                        string keyName = "cheat" + c.ToString() + "_desc";
                        if (!codes.Global.ContainsKey(keyName))
                            break;
                        string codeDesc = codes.Global[keyName].Replace("\"", "");
                        foreach (string code in activeCodes)
                            if (codeDesc == code)
                            {
                                codes.Global["cheat" + c.ToString() + "_enable"] = "\"true\"";
                                LogMessage(string.Format("Code: {0}", codeDesc));
                                continue;
                            }
                    }
                    var saveCodes = new FileIniDataParser();
                    saveCodes.WriteFile(codesOutPath, codes, System.Text.Encoding.ASCII);
                }
            }
            // Step 6 - Cleanup
            if (!buildCancel)
                RefreshProgress("Step 6/6: Cleaning up");

            // Delete data folder
            string directoryPath = Path.Combine(textBoxOutputPath.Text, "data");
            if (Directory.Exists(directoryPath))
            {
                int retries = 0;
                while (true)
                    try
                    {
                        File.Delete(Path.Combine(textBoxOutputPath.Text, "ip.bin"));
                        Directory.Delete(directoryPath, true);
                        LogMessage("Data folder deleted successfully.");
                        break;
                    }
                    catch (Exception ex)
                    {
                        if (retries > 19)
                        {
                            LogMessage(string.Format("Could not delete data folder: {0}", ex.Message.ToString()));
                            goto end;
                        }
                        retries++;
                        LogMessage(string.Format("Unable to delete data folder. Trying again (attempt {0} of 20)", retries.ToString()));
                        Thread.Sleep(1000);
                    }
            }
        end:
            LogMessage("");
            if (!buildCancel)
            {
                LogMessage(string.Format("The modified image is located at: {0}", textBoxOutputPath.Text));
                if (File.Exists(codesOutPath))
                    LogMessage(string.Format("Cheat file: {0}", codesOutPath));

                // Finish
                LogMessage("DONE!\n");
            }
            else
                LogMessage("Build cancelled");
            RefreshProgress("Click Build to rebuild a modified GDI or CDI image. Progress will be shown below.", false);
            CheckBuildButton();
            CheckEmulator();
            if (!buildCancel && autoRun)
            {
                buttonRunEmu_Click(null, null);
                autoRun = false;
            }
            buildCancel = false;
            building = false;
            if (logger != null)
            {
                logger.Flush();
                logger.Close();
                logger = null;
            }
        }

        void CopyDirectory(string sourceDir, string destinationDir, bool recursive, CancellationToken ct)
        {
            // https://learn.microsoft.com/en-us/dotnet/standard/io/how-to-copy-directories

            // Get information about the source directory
            var dir = new DirectoryInfo(sourceDir);

            // Check if the source directory exists
            if (!dir.Exists)
                throw new DirectoryNotFoundException($"Source directory not found: {dir.FullName}");

            // Cache directories before we start copying
            DirectoryInfo[] dirs = dir.GetDirectories();

            // Create the destination directory
            Directory.CreateDirectory(destinationDir);

            // Get the files in the source directory and copy to the destination directory
            foreach (FileInfo file in dir.GetFiles())
            {
                if (ct.IsCancellationRequested)
                {
                    return;
                }
                string targetFilePath = Path.Combine(destinationDir, file.Name);
                file.CopyTo(targetFilePath);
                LogMessage(string.Format("Copying file {0}", file.Name), false);
            }

            // If recursive and copying subdirectories, recursively call this method
            if (recursive)
            {
                foreach (DirectoryInfo subDir in dirs)
                {
                    string newDestinationDir = Path.Combine(destinationDir, subDir.Name);
                    CopyDirectory(subDir.FullName, newDestinationDir, true, ct);
                }
            }
        }
    }
}