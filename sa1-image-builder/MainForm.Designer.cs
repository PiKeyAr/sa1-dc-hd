﻿
namespace GUIPatcher
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            modDescription = new System.Windows.Forms.TextBox();
            tabControl1 = new System.Windows.Forms.TabControl();
            tabMods = new System.Windows.Forms.TabPage();
            buttonBottom = new System.Windows.Forms.Button();
            buttonDown = new System.Windows.Forms.Button();
            buttonUp = new System.Windows.Forms.Button();
            buttonTop = new System.Windows.Forms.Button();
            modListView = new System.Windows.Forms.ListView();
            columnName = new System.Windows.Forms.ColumnHeader();
            columnAuthor = new System.Windows.Forms.ColumnHeader();
            columnVersion = new System.Windows.Forms.ColumnHeader();
            columnChecked = new System.Windows.Forms.ColumnHeader();
            tabCodes = new System.Windows.Forms.TabPage();
            codesListView = new System.Windows.Forms.ListView();
            tabOptions = new System.Windows.Forms.TabPage();
            buttonBrowseEmu = new System.Windows.Forms.Button();
            textBoxEmuPath = new System.Windows.Forms.TextBox();
            label1 = new System.Windows.Forms.Label();
            labelGameCheckResult = new System.Windows.Forms.Label();
            buttonBrowse = new System.Windows.Forms.Button();
            textBoxOriginalPath = new System.Windows.Forms.TextBox();
            label3 = new System.Windows.Forms.Label();
            tabBuild = new System.Windows.Forms.TabPage();
            label4 = new System.Windows.Forms.Label();
            buttonBrowseOutput = new System.Windows.Forms.Button();
            textBoxOutputPath = new System.Windows.Forms.TextBox();
            labelStatus = new System.Windows.Forms.Label();
            textBoxLog = new System.Windows.Forms.TextBox();
            buttonBuild = new System.Windows.Forms.Button();
            buttonSaveSettings = new System.Windows.Forms.Button();
            timer1 = new System.Windows.Forms.Timer(components);
            buttonRunEmu = new System.Windows.Forms.Button();
            buttonBuildRun = new System.Windows.Forms.Button();
            buttonReload = new System.Windows.Forms.Button();
            buttonStop = new System.Windows.Forms.Button();
            checkBoxCDI = new System.Windows.Forms.CheckBox();
            tabControl1.SuspendLayout();
            tabMods.SuspendLayout();
            tabCodes.SuspendLayout();
            tabOptions.SuspendLayout();
            tabBuild.SuspendLayout();
            SuspendLayout();
            // 
            // modDescription
            // 
            modDescription.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            modDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            modDescription.Location = new System.Drawing.Point(0, 359);
            modDescription.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            modDescription.Multiline = true;
            modDescription.Name = "modDescription";
            modDescription.ReadOnly = true;
            modDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            modDescription.Size = new System.Drawing.Size(527, 144);
            modDescription.TabIndex = 4;
            modDescription.TabStop = false;
            modDescription.Text = "Mod description.";
            // 
            // tabControl1
            // 
            tabControl1.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            tabControl1.Controls.Add(tabMods);
            tabControl1.Controls.Add(tabCodes);
            tabControl1.Controls.Add(tabOptions);
            tabControl1.Controls.Add(tabBuild);
            tabControl1.Location = new System.Drawing.Point(9, 9);
            tabControl1.Margin = new System.Windows.Forms.Padding(2);
            tabControl1.Name = "tabControl1";
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new System.Drawing.Size(540, 537);
            tabControl1.TabIndex = 0;
            // 
            // tabMods
            // 
            tabMods.BackColor = System.Drawing.SystemColors.Control;
            tabMods.Controls.Add(buttonBottom);
            tabMods.Controls.Add(buttonDown);
            tabMods.Controls.Add(buttonUp);
            tabMods.Controls.Add(buttonTop);
            tabMods.Controls.Add(modListView);
            tabMods.Controls.Add(modDescription);
            tabMods.Location = new System.Drawing.Point(4, 24);
            tabMods.Margin = new System.Windows.Forms.Padding(2);
            tabMods.Name = "tabMods";
            tabMods.Padding = new System.Windows.Forms.Padding(2);
            tabMods.Size = new System.Drawing.Size(532, 509);
            tabMods.TabIndex = 0;
            tabMods.Text = "Mods";
            // 
            // buttonBottom
            // 
            buttonBottom.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBottom.Location = new System.Drawing.Point(497, 113);
            buttonBottom.Name = "buttonBottom";
            buttonBottom.Size = new System.Drawing.Size(30, 30);
            buttonBottom.TabIndex = 10;
            buttonBottom.Text = "↓↓";
            buttonBottom.UseVisualStyleBackColor = true;
            buttonBottom.Click += buttonBottom_Click;
            // 
            // buttonDown
            // 
            buttonDown.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonDown.Location = new System.Drawing.Point(497, 77);
            buttonDown.Name = "buttonDown";
            buttonDown.Size = new System.Drawing.Size(30, 30);
            buttonDown.TabIndex = 9;
            buttonDown.Text = "↓";
            buttonDown.UseVisualStyleBackColor = true;
            buttonDown.Click += buttonDown_Click;
            // 
            // buttonUp
            // 
            buttonUp.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonUp.Location = new System.Drawing.Point(497, 41);
            buttonUp.Name = "buttonUp";
            buttonUp.Size = new System.Drawing.Size(30, 30);
            buttonUp.TabIndex = 8;
            buttonUp.Text = "↑";
            buttonUp.UseVisualStyleBackColor = true;
            buttonUp.Click += buttonUp_Click;
            // 
            // buttonTop
            // 
            buttonTop.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonTop.Location = new System.Drawing.Point(497, 5);
            buttonTop.Name = "buttonTop";
            buttonTop.Size = new System.Drawing.Size(30, 30);
            buttonTop.TabIndex = 7;
            buttonTop.Text = "↑↑";
            buttonTop.UseVisualStyleBackColor = true;
            buttonTop.Click += buttonTop_Click;
            // 
            // modListView
            // 
            modListView.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            modListView.CheckBoxes = true;
            modListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] { columnName, columnAuthor, columnVersion, columnChecked });
            modListView.FullRowSelect = true;
            modListView.Location = new System.Drawing.Point(2, 2);
            modListView.Margin = new System.Windows.Forms.Padding(2);
            modListView.MultiSelect = false;
            modListView.Name = "modListView";
            modListView.Size = new System.Drawing.Size(490, 351);
            modListView.Sorting = System.Windows.Forms.SortOrder.Ascending;
            modListView.TabIndex = 6;
            modListView.UseCompatibleStateImageBehavior = false;
            modListView.View = System.Windows.Forms.View.Details;
            modListView.ColumnClick += modListView_ColumnClick;
            modListView.ItemChecked += modListView_ItemChecked;
            modListView.SelectedIndexChanged += modListView_SelectedIndexChanged;
            // 
            // columnName
            // 
            columnName.Text = "Name";
            columnName.Width = 250;
            // 
            // columnAuthor
            // 
            columnAuthor.Text = "Author";
            columnAuthor.Width = 145;
            // 
            // columnVersion
            // 
            columnVersion.Text = "Version";
            columnVersion.Width = 99;
            // 
            // columnChecked
            // 
            columnChecked.Text = "Active";
            columnChecked.Width = 90;
            // 
            // tabCodes
            // 
            tabCodes.BackColor = System.Drawing.SystemColors.Control;
            tabCodes.Controls.Add(codesListView);
            tabCodes.Location = new System.Drawing.Point(4, 24);
            tabCodes.Margin = new System.Windows.Forms.Padding(2);
            tabCodes.Name = "tabCodes";
            tabCodes.Padding = new System.Windows.Forms.Padding(2);
            tabCodes.Size = new System.Drawing.Size(532, 509);
            tabCodes.TabIndex = 3;
            tabCodes.Text = "Codes";
            // 
            // codesListView
            // 
            codesListView.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            codesListView.CheckBoxes = true;
            codesListView.FullRowSelect = true;
            codesListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            codesListView.Location = new System.Drawing.Point(2, 2);
            codesListView.Margin = new System.Windows.Forms.Padding(2);
            codesListView.MultiSelect = false;
            codesListView.Name = "codesListView";
            codesListView.Size = new System.Drawing.Size(526, 503);
            codesListView.TabIndex = 7;
            codesListView.UseCompatibleStateImageBehavior = false;
            codesListView.View = System.Windows.Forms.View.List;
            // 
            // tabOptions
            // 
            tabOptions.BackColor = System.Drawing.SystemColors.Control;
            tabOptions.Controls.Add(buttonBrowseEmu);
            tabOptions.Controls.Add(textBoxEmuPath);
            tabOptions.Controls.Add(label1);
            tabOptions.Controls.Add(labelGameCheckResult);
            tabOptions.Controls.Add(buttonBrowse);
            tabOptions.Controls.Add(textBoxOriginalPath);
            tabOptions.Controls.Add(label3);
            tabOptions.Location = new System.Drawing.Point(4, 24);
            tabOptions.Margin = new System.Windows.Forms.Padding(2);
            tabOptions.Name = "tabOptions";
            tabOptions.Padding = new System.Windows.Forms.Padding(2);
            tabOptions.Size = new System.Drawing.Size(532, 509);
            tabOptions.TabIndex = 1;
            tabOptions.Text = "Options";
            // 
            // buttonBrowseEmu
            // 
            buttonBrowseEmu.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowseEmu.Location = new System.Drawing.Point(429, 229);
            buttonBrowseEmu.Name = "buttonBrowseEmu";
            buttonBrowseEmu.Size = new System.Drawing.Size(79, 28);
            buttonBrowseEmu.TabIndex = 4;
            buttonBrowseEmu.Text = "Browse...";
            buttonBrowseEmu.UseVisualStyleBackColor = true;
            buttonBrowseEmu.Click += buttonBrowseEmu_Click;
            // 
            // textBoxEmuPath
            // 
            textBoxEmuPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxEmuPath.Location = new System.Drawing.Point(15, 231);
            textBoxEmuPath.Name = "textBoxEmuPath";
            textBoxEmuPath.Size = new System.Drawing.Size(403, 23);
            textBoxEmuPath.TabIndex = 3;
            textBoxEmuPath.TextChanged += textBoxEmuPath_TextChanged;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new System.Drawing.Point(15, 200);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(291, 15);
            label1.TabIndex = 4;
            label1.Text = "Location of the Dreamcast emulator to run the image:";
            // 
            // labelGameCheckResult
            // 
            labelGameCheckResult.AutoSize = true;
            labelGameCheckResult.Location = new System.Drawing.Point(13, 121);
            labelGameCheckResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelGameCheckResult.Name = "labelGameCheckResult";
            labelGameCheckResult.Size = new System.Drawing.Size(392, 15);
            labelGameCheckResult.TabIndex = 3;
            labelGameCheckResult.Text = "Browse for a folder to check whether it is the correct version of the game.";
            // 
            // buttonBrowse
            // 
            buttonBrowse.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowse.Location = new System.Drawing.Point(429, 72);
            buttonBrowse.Margin = new System.Windows.Forms.Padding(2);
            buttonBrowse.Name = "buttonBrowse";
            buttonBrowse.Size = new System.Drawing.Size(79, 28);
            buttonBrowse.TabIndex = 2;
            buttonBrowse.Text = "Browse...";
            buttonBrowse.UseVisualStyleBackColor = true;
            buttonBrowse.Click += buttonBrowse_Click;
            // 
            // textBoxOriginalPath
            // 
            textBoxOriginalPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxOriginalPath.Location = new System.Drawing.Point(15, 76);
            textBoxOriginalPath.Margin = new System.Windows.Forms.Padding(2);
            textBoxOriginalPath.Name = "textBoxOriginalPath";
            textBoxOriginalPath.Size = new System.Drawing.Size(403, 23);
            textBoxOriginalPath.TabIndex = 1;
            textBoxOriginalPath.TextChanged += textBoxOriginalPath_TextChanged;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new System.Drawing.Point(13, 11);
            label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(494, 45);
            label3.TabIndex = 0;
            label3.Text = "Select the folder containing the original Sonic Adventure (US 1.005 or Autodemo) GDI file:\r\n\r\nThis program will create a patched GDI or CDI image of Sonic Adventure with selected mods.\r\n";
            // 
            // tabBuild
            // 
            tabBuild.BackColor = System.Drawing.SystemColors.Control;
            tabBuild.Controls.Add(label4);
            tabBuild.Controls.Add(buttonBrowseOutput);
            tabBuild.Controls.Add(textBoxOutputPath);
            tabBuild.Controls.Add(labelStatus);
            tabBuild.Controls.Add(textBoxLog);
            tabBuild.Location = new System.Drawing.Point(4, 24);
            tabBuild.Margin = new System.Windows.Forms.Padding(2);
            tabBuild.Name = "tabBuild";
            tabBuild.Padding = new System.Windows.Forms.Padding(2);
            tabBuild.Size = new System.Drawing.Size(532, 509);
            tabBuild.TabIndex = 2;
            tabBuild.Text = "Build Image";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new System.Drawing.Point(6, 9);
            label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(247, 15);
            label4.TabIndex = 9;
            label4.Text = "Select the output path for the patched image:";
            // 
            // buttonBrowseOutput
            // 
            buttonBrowseOutput.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            buttonBrowseOutput.Location = new System.Drawing.Point(446, 23);
            buttonBrowseOutput.Margin = new System.Windows.Forms.Padding(2);
            buttonBrowseOutput.Name = "buttonBrowseOutput";
            buttonBrowseOutput.Size = new System.Drawing.Size(79, 28);
            buttonBrowseOutput.TabIndex = 1;
            buttonBrowseOutput.Text = "Browse...";
            buttonBrowseOutput.UseVisualStyleBackColor = true;
            buttonBrowseOutput.Click += buttonBrowseOutput_Click;
            // 
            // textBoxOutputPath
            // 
            textBoxOutputPath.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxOutputPath.Location = new System.Drawing.Point(9, 26);
            textBoxOutputPath.Margin = new System.Windows.Forms.Padding(2);
            textBoxOutputPath.Name = "textBoxOutputPath";
            textBoxOutputPath.Size = new System.Drawing.Size(432, 23);
            textBoxOutputPath.TabIndex = 0;
            textBoxOutputPath.TextChanged += textBoxOutputPath_TextChanged;
            // 
            // labelStatus
            // 
            labelStatus.AutoSize = true;
            labelStatus.Location = new System.Drawing.Point(6, 64);
            labelStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            labelStatus.Name = "labelStatus";
            labelStatus.Size = new System.Drawing.Size(423, 15);
            labelStatus.TabIndex = 6;
            labelStatus.Text = "Click Build to create a modified GD-ROM image. Progress will be shown below.";
            // 
            // textBoxLog
            // 
            textBoxLog.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            textBoxLog.Location = new System.Drawing.Point(6, 82);
            textBoxLog.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            textBoxLog.Multiline = true;
            textBoxLog.Name = "textBoxLog";
            textBoxLog.ReadOnly = true;
            textBoxLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            textBoxLog.Size = new System.Drawing.Size(520, 421);
            textBoxLog.TabIndex = 2;
            // 
            // buttonBuild
            // 
            buttonBuild.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonBuild.Enabled = false;
            buttonBuild.Location = new System.Drawing.Point(371, 549);
            buttonBuild.Margin = new System.Windows.Forms.Padding(2);
            buttonBuild.Name = "buttonBuild";
            buttonBuild.Size = new System.Drawing.Size(84, 28);
            buttonBuild.TabIndex = 6;
            buttonBuild.Text = "Build";
            buttonBuild.UseVisualStyleBackColor = true;
            buttonBuild.Click += buttonBuild_Click;
            // 
            // buttonSaveSettings
            // 
            buttonSaveSettings.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonSaveSettings.Location = new System.Drawing.Point(89, 549);
            buttonSaveSettings.Margin = new System.Windows.Forms.Padding(2);
            buttonSaveSettings.Name = "buttonSaveSettings";
            buttonSaveSettings.Size = new System.Drawing.Size(72, 28);
            buttonSaveSettings.TabIndex = 2;
            buttonSaveSettings.Text = "Save";
            buttonSaveSettings.UseVisualStyleBackColor = true;
            buttonSaveSettings.Click += buttonSaveSettings_Click;
            // 
            // timer1
            // 
            timer1.Interval = 60;
            timer1.Tick += timer1_Tick;
            // 
            // buttonRunEmu
            // 
            buttonRunEmu.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonRunEmu.Enabled = false;
            buttonRunEmu.Location = new System.Drawing.Point(166, 549);
            buttonRunEmu.Name = "buttonRunEmu";
            buttonRunEmu.Size = new System.Drawing.Size(72, 28);
            buttonRunEmu.TabIndex = 3;
            buttonRunEmu.Text = "Run";
            buttonRunEmu.UseVisualStyleBackColor = true;
            buttonRunEmu.Click += buttonRunEmu_Click;
            // 
            // buttonBuildRun
            // 
            buttonBuildRun.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            buttonBuildRun.Enabled = false;
            buttonBuildRun.Location = new System.Drawing.Point(460, 549);
            buttonBuildRun.Name = "buttonBuildRun";
            buttonBuildRun.Size = new System.Drawing.Size(84, 28);
            buttonBuildRun.TabIndex = 7;
            buttonBuildRun.Text = "Build && Run";
            buttonBuildRun.UseVisualStyleBackColor = true;
            buttonBuildRun.Click += buttonBuildRun_Click;
            // 
            // buttonReload
            // 
            buttonReload.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonReload.Location = new System.Drawing.Point(12, 549);
            buttonReload.Name = "buttonReload";
            buttonReload.Size = new System.Drawing.Size(72, 28);
            buttonReload.TabIndex = 1;
            buttonReload.Text = "Reload";
            buttonReload.UseVisualStyleBackColor = true;
            buttonReload.Click += buttonReload_Click;
            // 
            // buttonStop
            // 
            buttonStop.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            buttonStop.Location = new System.Drawing.Point(244, 549);
            buttonStop.Name = "buttonStop";
            buttonStop.Size = new System.Drawing.Size(72, 28);
            buttonStop.TabIndex = 4;
            buttonStop.Text = "Stop";
            buttonStop.UseVisualStyleBackColor = true;
            buttonStop.Click += buttonStop_Click;
            // 
            // checkBoxCDI
            // 
            checkBoxCDI.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            checkBoxCDI.AutoSize = true;
            checkBoxCDI.Location = new System.Drawing.Point(322, 555);
            checkBoxCDI.Name = "checkBoxCDI";
            checkBoxCDI.Size = new System.Drawing.Size(45, 19);
            checkBoxCDI.TabIndex = 5;
            checkBoxCDI.Text = "CDI";
            checkBoxCDI.UseVisualStyleBackColor = true;
            checkBoxCDI.CheckedChanged += radioButtonCDI_CheckedChanged;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            ClientSize = new System.Drawing.Size(556, 589);
            Controls.Add(checkBoxCDI);
            Controls.Add(buttonStop);
            Controls.Add(buttonReload);
            Controls.Add(buttonBuildRun);
            Controls.Add(buttonRunEmu);
            Controls.Add(buttonBuild);
            Controls.Add(tabControl1);
            Controls.Add(buttonSaveSettings);
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            Margin = new System.Windows.Forms.Padding(2);
            Name = "MainForm";
            StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            Text = "Sonic Adventure Image Builder";
            tabControl1.ResumeLayout(false);
            tabMods.ResumeLayout(false);
            tabMods.PerformLayout();
            tabCodes.ResumeLayout(false);
            tabOptions.ResumeLayout(false);
            tabOptions.PerformLayout();
            tabBuild.ResumeLayout(false);
            tabBuild.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private System.Windows.Forms.TextBox modDescription;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMods;
        private System.Windows.Forms.TabPage tabOptions;
        private System.Windows.Forms.Button buttonBrowse;
        private System.Windows.Forms.TextBox textBoxOriginalPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabPage tabBuild;
        private System.Windows.Forms.Button buttonBrowseOutput;
        private System.Windows.Forms.TextBox textBoxOutputPath;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.TextBox textBoxLog;
        private System.Windows.Forms.Label labelGameCheckResult;
        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ListView modListView;
        private System.Windows.Forms.ColumnHeader columnName;
        private System.Windows.Forms.ColumnHeader columnAuthor;
        private System.Windows.Forms.ColumnHeader columnVersion;
        private System.Windows.Forms.Button buttonSaveSettings;
        private System.Windows.Forms.TabPage tabCodes;
        private System.Windows.Forms.ListView codesListView;
        private System.Windows.Forms.Button buttonBrowseEmu;
        private System.Windows.Forms.TextBox textBoxEmuPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonRunEmu;
        private System.Windows.Forms.Button buttonBuildRun;
        private System.Windows.Forms.ColumnHeader columnChecked;
        private System.Windows.Forms.Button buttonReload;
        private System.Windows.Forms.Button buttonBottom;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Button buttonTop;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.CheckBox checkBoxCDI;
    }
}

