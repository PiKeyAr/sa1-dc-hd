﻿using GUIPatcher.Properties;
using IniParser;
using IniParser.Model;
using Ookii.Dialogs.WinForms;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUIPatcher
{
    public partial class MainForm : Form
    {
        // Patcher settings
        public IniData settings;
        public IniData codes;
        public List<Mod> mods;
        public List<string> activeMods;
        public List<string> activeCodes;
        private readonly ListViewColumnSorter lvwColumnSorter;

        // Work folders
        public string currentDir;
        public string currentDirAss; // Patcher.exe location

        static List<string> PRSFiles = new(); // List of PRS files to repack during build process

        public TextBoxWriter writer; // Console redirection

        public bool autodemoMode; // Using Autodemo image
        public bool autoRun; // Run the emulator after build
        public bool building; // Build running
        public bool buildCancel; // Build cancelled

        // Build cancellation token
        CancellationTokenSource cancellationTokenSource;
        CancellationToken cancellationToken;

        public static TextWriter logger; // File log

        // SA1 versions to check for compatibility
        enum SA1Version
        {
            US_1005,
            JP_International,
            US_1004,
            JP_Original,
            EU_1003,
            US_Limited,
            E3_Trial,
            Autodemo,
            JP_Taikenban,
            Unknown
        }

        public MainForm()
        {
            InitializeComponent();
            // ListView stuff
            lvwColumnSorter = new ListViewColumnSorter();
            modListView.ListViewItemSorter = lvwColumnSorter;
            // Get the path where the assembly is loaded with built-in stuff
            currentDirAss = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            // Use external mods folder if available, otherwise use built-in mods
            if (Directory.Exists(Path.Combine(Environment.CurrentDirectory, "mods")))
                currentDir = Environment.CurrentDirectory;
            else
                currentDir = currentDirAss;
            activeMods = new List<string>();
            activeCodes = new List<string>();
            tabMods.Enabled = tabCodes.Enabled = false;
            if (CheckSA1Version(textBoxOriginalPath.Text))
                tabBuild.Enabled = false;
            mods = new List<Mod>();
            modListView.Items.Clear();
            ReloadMods();
            LoadSettings();
        }

        // Reload mods when a different GDI is selected
        private void SwitchImage()
        {
            ReloadMods();
            InitializeCodes();
            modListView.Items.Clear();
            activeMods.Clear();
            CheckMods();
            PutCheckedModsFirst();
        }

        // Check if the mods exists and add checkboxes to the ListView
        private void CheckMods()
        {
            // Load codes
            if (settings.Global["Codes"] != null)
                activeCodes = settings.Global["Codes"].Split(',').ToList();
            // Load mods
            if (settings.Global["Mods"] != null)
            {
                string[] activeModListINI = settings.Global["Mods"].Split(',');
                for (int i = 0; i < activeModListINI.Length; i++)
                {
                    bool missing = true;
                    if (activeModListINI[i] != "")
                    {
                        foreach (Mod mod in mods)
                            if (mod.Name == activeModListINI[i])
                            {
                                missing = false;
                                activeMods.Add(activeModListINI[i]);
                                if (mod.RequiredCodes != null)
                                    for (int c = 0; c < mod.RequiredCodes.Length; c++)
                                    {
                                        if (!activeCodes.Contains(mod.RequiredCodes[c]))
                                            activeCodes.Add(mod.RequiredCodes[c]);
                                    }
                            }
                    }
                    else
                        missing = false;
                    if (missing)
                    {
                        MessageBox.Show("Mod '" + activeModListINI[i] + "' is missing.\n\nIt will be removed from the active mod list.");
                    }
                }
            }
            // Check mods
            foreach (Mod mod in mods)
            {
                if (mod.Autodemo == autodemoMode)
                    modListView.Items.Add(new ListViewItem(new[] { mod.Name, mod.Author, mod.Version, activeMods.Contains(mod.Name) ? "Yes" : "No" }) { Checked = activeMods.Contains(mod.Name) ? true : false });
            }
            // Initialize and check codes
            InitializeCodes();
        }

        // Load the settings ini file
        private void LoadSettings()
        {
            if (!File.Exists("settings.ini"))
            {
                settings = new IniData();
                return;
            }
            // Load INI
            var ini = new FileIniDataParser();
            settings = ini.ReadFile(Path.Combine(Environment.CurrentDirectory, "settings.ini"));
            // Load paths
            textBoxOriginalPath.Text = settings.Global["OriginalPath"];
            textBoxOutputPath.Text = settings.Global["OutputPath"];
            textBoxEmuPath.Text = settings.Global["EmuPath"];
            if (settings.Global["BuildCDI"] != null)
            {
                bool res = bool.TryParse(settings.Global["BuildCDI"], out bool cdi);
                if (res)
                {
                    checkBoxCDI.Checked = cdi;
                }
                else
                {
                    checkBoxCDI.Checked = true;
                }
            }
        }

        // Save the settings ini file
        private void SaveSettings()
        {
            IniData settings = new IniData();
            settings.Global["BuildCDI"] = checkBoxCDI.Checked ? "true" : "false";
            // Save paths
            settings.Global["OriginalPath"] = textBoxOriginalPath.Text;
            settings.Global["OutputPath"] = textBoxOutputPath.Text;
            settings.Global["EmuPath"] = textBoxEmuPath.Text;
            // Save mods
            activeMods.Clear();
            activeCodes.Clear();
            for (int c = 0; c < modListView.Items.Count; c++)
            {
                if (modListView.Items[c].Checked)
                    activeMods.Add(modListView.Items[c].Text);
            }
            settings.Global["Mods"] = string.Join(",", activeMods);
            // Save codes
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Checked)
                    activeCodes.Add(codesListView.Items[c].Text);
            }
            settings.Global["Codes"] = string.Join(",", activeCodes);
            // Write INI
            var ini = new FileIniDataParser();
            ini.WriteFile(Path.Combine(Environment.CurrentDirectory, "settings.ini"), settings);
            PutCheckedModsFirst();
        }

        // Clear and readd codes
        private void InitializeCodes()
        {
            activeCodes.Clear();
            codesListView.Items.Clear();
            var ini = new FileIniDataParser();
            // Default to internal codes
            string codesPath = Path.Combine(currentDirAss, "mods", autodemoMode ? "codes_ad.cht" : "codes.cht");
            // If external codes exists, load them instead
            if (File.Exists(Path.Combine(Environment.CurrentDirectory, "mods", autodemoMode ? "codes_ad.cht" : "codes.cht")))
                codesPath = Path.Combine(Environment.CurrentDirectory, "mods", autodemoMode ? "codes_ad.cht" : "codes.cht");
            if (!File.Exists(codesPath))
            {
                tabControl1.TabPages.Remove(tabCodes);
                return;
            }
            else
            {
                if (!tabControl1.TabPages.Contains(tabCodes))
                    tabControl1.TabPages.Insert(1, tabCodes);
            }
            codes = ini.ReadFile(codesPath);
            for (int c = 0; c < 99; c++)
            {
                string keyName = "cheat" + c.ToString() + "_desc";
                if (codes.Global.ContainsKey(keyName))
                {
                    string codeDesc = codes.Global[keyName].Replace("\"", "");
                    //MessageBox.Show(codes.Global[keyName]);
                    codesListView.Items.Add(new ListViewItem(new[] { codeDesc }) { Checked = activeCodes.Contains(codeDesc) ? true : false });
                }
                else
                    break;
            }
        }

        // Hack to set mod description for the currently selected mod
        private string FidModDescByName(string name)
        {
            foreach (Mod mod in mods)
            {
                if (mod.Name == name)
                    return mod.Description;
            }
            return "Mod description.";
        }

        private void modListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (modListView.SelectedItems.Count > 0)
                modDescription.Text = FidModDescByName(modListView.SelectedItems[0].Text).Replace("\\n", System.Environment.NewLine);
        }

        // Get the version of SA1 for the selected GDI
        private bool CheckSA1Version(string path)
        {
            SA1Version result;
            bool correct = false;
            string track01path = Path.Combine(path, "track01.bin");
            if (!File.Exists(track01path))
            {
                labelGameCheckResult.Text = "Could not find track01.bin in the selected folder.\n\nMake sure the selected folder contains a GDI file with all tracks.";
                return false;
            }
            byte[] track01 = File.ReadAllBytes(track01path);
            byte[] ver_b = new byte[13];
            for (int n = 0; n < 13; n++)
            {
                ver_b[n] = track01[0x5B + n];
            }
            string ver = Encoding.ASCII.GetString(ver_b);
            switch (ver)
            {
                case "1.00419990812":
                    result = SA1Version.US_1004;
                    break;
                case "1.00519991005":
                    result = SA1Version.US_1005;
                    autodemoMode = false;
                    correct = true;
                    Icon = Resources.iconSA1;
                    Text = "Sonic Adventure Image Builder";
                    break;
                case "1.00219990302":
                    result = SA1Version.JP_Taikenban;
                    break;
                case "1.00219990604":
                    result = SA1Version.US_Limited;
                    break;
                case "1.00719981210":
                    result = SA1Version.JP_Original;
                    break;
                case "1.00319990920":
                    result = SA1Version.JP_International;
                    break;
                case "1.00319990909":
                    result = SA1Version.EU_1003;
                    break;
                case "1.00019990608":
                    result = SA1Version.E3_Trial;
                    break;
                case "1.00019981016":
                    result = SA1Version.Autodemo;
                    correct = true;
                    autodemoMode = true;
                    Icon = Resources.iconAD;
                    Text = "SA Autodemo Image Builder";
                    break;
                default:
                    result = SA1Version.Unknown;
                    break;
            }
            string correctstring = correct ? "This is the correct version of the game. You can now use the Mods and Build Image tabs." : "This is the wrong version of the game.";
            labelGameCheckResult.Text = (correctstring + System.Environment.NewLine + System.Environment.NewLine + "Game version: " + result.ToString() + System.Environment.NewLine + "Version string: " + ver);
            return correct;
        }

        // Check if the supplied path for the GDI is valid
        private void textBoxOriginalPath_TextChanged(object sender, EventArgs e)
        {
            bool valid = CheckSA1Version(textBoxOriginalPath.Text);
            tabMods.Enabled = tabCodes.Enabled = tabBuild.Enabled = valid;
            SwitchImage();
            CheckBuildButton();
        }

        // Locate the GDI using the dialog
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            VistaFolderBrowserDialog dlg = new VistaFolderBrowserDialog();
            if (textBoxOriginalPath.Text != "")
                dlg.SelectedPath = textBoxOriginalPath.Text;
            else
                dlg.SelectedPath = Environment.CurrentDirectory;
            dlg.ShowNewFolderButton = false;
            if (dlg.ShowDialog() == DialogResult.OK)
                textBoxOriginalPath.Text = dlg.SelectedPath;
            // Switch tabs automatically
            bool valid = CheckSA1Version(textBoxOriginalPath.Text);
            if (valid)
                tabControl1.SelectTab(tabMods);
            else
                tabControl1.SelectTab(tabOptions);
        }

        // Enable or disable "Build" and "Build and Run" buttons

        private void CheckBuildButton()
        {
            if (buttonBuild.InvokeRequired)
            {
                buttonBuild.Invoke(new MethodInvoker(() => { CheckBuildButton(); }));
            }
            if (textBoxOutputPath.Text != "" && CheckSA1Version(textBoxOriginalPath.Text))
            {
                buttonBuild.Enabled = true;
                CheckEmulator();
            }
            else
                buttonBuild.Enabled = buttonBuildRun.Enabled = false;
        }

        private void textBoxOutputPath_TextChanged(object sender, EventArgs e)
        {
            CheckBuildButton();
        }

        // Report progress
        void RefreshProgress(string step, bool console = true)
        {
            if (labelStatus.InvokeRequired)
            {
                labelStatus.Invoke(new MethodInvoker(() => { RefreshProgress(step, console); }));
            }
            else
            {
                labelStatus.Text = step;
                if (console)
                {
                    LogMessage("");
                    LogMessage(step);
                }
            }
        }

        // Set a code as active
        private void EnableCode(string code)
        {
            activeCodes.Add(code);
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Text == code)
                {
                    codesListView.Items[c].Checked = true;
                    break;
                }
            }
        }

        // Write a line to the log file and optionally output it to the textbox
        private void LogMessage(string message, bool display = true)
        {
            if (logger != null)
            {
                logger.WriteLine(message, display);
                logger.Flush();
            }
            if (display)
                Console.WriteLine(message);
        }

        // Recheck mods and codes before building
        private void RearrangeActiveModsAndCodes()
        {
            // Recheck current mods
            activeMods.Clear();
            for (int c = 0; c < modListView.Items.Count; c++)
            {
                if (modListView.Items[c].Checked)
                    activeMods.Add(modListView.Items[c].Text);
            }
            // Recheck current codes
            activeCodes.Clear();
            for (int c = 0; c < codesListView.Items.Count; c++)
            {
                if (codesListView.Items[c].Checked)
                    activeCodes.Add(codesListView.Items[c].Text);
            }
        }

        // Start build process
        private async void buttonBuild_Click(object sender, EventArgs e)
        {
            SaveSettings();
            if (Path.GetFullPath(textBoxOriginalPath.Text) == Path.GetFullPath(textBoxOutputPath.Text))
            {
                MessageBox.Show(this, "Source and destination paths cannot be the same.", "SA1 Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            tabControl1.SelectTab(tabBuild);
            textBoxLog.Text = "";
            buttonBuild.Enabled = buttonBuildRun.Enabled = buttonRunEmu.Enabled = false;
            // Reload mods in case they were modified while the program was running
            ReloadMods();
            RearrangeActiveModsAndCodes();
            // Force enable codes
            List<Mod> applyMods = new List<Mod>();
            for (int i = 0; i < mods.Count; i++)
                for (int u = 0; u < activeMods.Count; u++)
                    if (mods[i].Name == activeMods[u])
                        applyMods.Add(mods[i]);
            foreach (Mod data in applyMods)
                if (data.RequiredCodes != null)
                    for (int c = 0; c < data.RequiredCodes.Length; c++)
                        EnableCode(data.RequiredCodes[c]);
            writer = new TextBoxWriter(textBoxLog);
            timer1.Enabled = true;
            Console.SetOut(writer);
            Console.WriteLine("Build process started");
            cancellationTokenSource = new CancellationTokenSource();
            cancellationToken = cancellationTokenSource.Token;
            Task tc = Task.Run(() => BuildStart(cancellationToken), cancellationToken);
            await tc;
        }

        // Clear the mods list and readd mods
        private void ReloadMods()
        {
            mods.Clear();
            string[] inifiles = Directory.GetFiles(Path.GetFullPath(Path.Combine(currentDir, "mods")), "mod.ini", SearchOption.AllDirectories);
            for (int i = 0; i < inifiles.Length; i++)
            {
                Mod mod = new Mod(inifiles[i]);
                if (mod.Autodemo == autodemoMode)
                    mods.Add(mod);
            }
        }

        // Select output folder
        private void buttonBrowseOutput_Click(object sender, EventArgs e)
        {
            VistaFolderBrowserDialog dlg = new VistaFolderBrowserDialog();
            if (textBoxOutputPath.Text != "")
                dlg.SelectedPath = textBoxOutputPath.Text;
            else
                dlg.SelectedPath = Environment.CurrentDirectory;
            dlg.ShowNewFolderButton = true;
            if (dlg.ShowDialog() == DialogResult.OK)
                textBoxOutputPath.Text = dlg.SelectedPath;
            CheckBuildButton();
        }

        // Write the console
        private void timer1_Tick(object sender, EventArgs e)
        {
            writer?.WriteOut();
        }

        private void buttonSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
        }

        // Kill process (generic version used in addition to the async one)
        private void TerminateProcess(string prcname)
        {
            Process[] processes = Process.GetProcessesByName(prcname);
            foreach (Process prc in processes)
            {
                Console.WriteLine("Terminating process: {0}", prcname);
                prc.Kill();
                prc.WaitForExit();
                prc.Dispose();
            }
        }

        // Kill emulator process
        private void TerminateEmu()
        {
            string emuname = Path.GetFileNameWithoutExtension(textBoxEmuPath.Text).ToLowerInvariant();
            TerminateProcess(emuname);
        }

        // Run emulator
        private async void buttonRunEmu_Click(object sender, EventArgs e)
        {
            string emuname = Path.GetFileNameWithoutExtension(textBoxEmuPath.Text).ToLowerInvariant();
            TerminateEmu();
            Console.WriteLine("Running emulator...");
            string args;
            string imgname = checkBoxCDI.Checked ? "disc.cdi" : "disc.gdi";

            switch (emuname)
            {
                case "flycast":
                case "redream":
                    args = "\"" + Path.Combine(textBoxOutputPath.Text, imgname) + "\"";
                    break;
                case "demul":
                default:
                    args = "-run=dc -image=\"" + Path.Combine(textBoxOutputPath.Text, imgname) + "\"";
                    break;
            }
            try
            {
                var exitCode = await ProcessStuff.StartProcess(textBoxEmuPath.Text, args, new CancellationTokenSource(), Path.GetDirectoryName(textBoxEmuPath.Text), null, null, null);
                //Console.WriteLine("Process {0} exited with Exit Code {1}", Path.GetFileName(textBoxEmuPath.Text), exitCode);
            }
            catch (TaskCanceledException)
            {
                Console.WriteLine("Process killed");
            }
        }

        // Check if the emulator path is valid and enable the button
        private void CheckEmulator()
        {
            if (!File.Exists(textBoxEmuPath.Text))
            {
                buttonBuildRun.Enabled = buttonRunEmu.Enabled = false;
                return;
            }
            buttonBuildRun.Enabled = true;
            buttonRunEmu.Enabled = File.Exists(Path.Combine(textBoxOutputPath.Text, "disc." + (checkBoxCDI.Checked ? "cdi" : "gdi")));
        }

        private void textBoxEmuPath_TextChanged(object sender, EventArgs e)
        {
            CheckEmulator();
        }

        private void radioButtonCDI_CheckedChanged(object sender, EventArgs e)
        {
            CheckEmulator();
        }

        private void buttonBrowseEmu_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog { Filter = "EXE files|*.exe" })
            {
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    textBoxEmuPath.Text = ofd.FileName;
                }
            }
        }

        private void buttonBuildRun_Click(object sender, EventArgs e)
        {
            if (Path.GetFullPath(textBoxOriginalPath.Text) == Path.GetFullPath(textBoxOutputPath.Text))
            {
                MessageBox.Show(this, "Source and destination paths cannot be the same.", "SA1 Image Builder Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            buttonBuildRun.Enabled = false;
            autoRun = true;
            buttonBuild_Click(sender, e);
        }

        private void PutCheckedModsFirst()
        {
            lvwColumnSorter.Order = SortOrder.Descending;
            lvwColumnSorter.SortColumn = 3;
            modListView.Sort();
            for (int m = activeMods.Count - 1; m >= 0; m--)
            {
                string modName = activeMods[m];
                //MessageBox.Show(modName);
                for (int item = 0; item < modListView.Items.Count; item++)
                    if (modListView.Items[item].Text == modName)
                    {
                        var listitem = modListView.Items[item];
                        modListView.Items.RemoveAt(item);
                        modListView.Items.Insert(0, listitem);
                    }
            }
        }

        private void modListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (e.Column == 3)
            {
                PutCheckedModsFirst();
                return;
            }
            // https://learn.microsoft.com/en-us/troubleshoot/developer/visualstudio/csharp/language-compilers/sort-listview-by-column
            // Determine if clicked column is already the column that is being sorted.
            if (e.Column == lvwColumnSorter.SortColumn)
            {

                // Reverse the current sort direction for this column.
                if (lvwColumnSorter.Order == SortOrder.Ascending)
                {
                    lvwColumnSorter.Order = SortOrder.Descending;
                }
                else
                {
                    lvwColumnSorter.Order = SortOrder.Ascending;
                }
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvwColumnSorter.SortColumn = e.Column;
                lvwColumnSorter.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            modListView.Sort();
        }

        private void modListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            e.Item.SubItems[3].Text = e.Item.Checked ? "Yes" : "No";
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            LoadSettings();
            ReloadMods();
            SwitchImage();
        }

        private void buttonTop_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            var item = modListView.SelectedItems[0];
            modListView.Items.RemoveAt(modListView.SelectedIndices[0]);
            modListView.Items.Insert(0, item);
            modListView.Items[0].EnsureVisible();
        }

        private void buttonUp_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            int index = modListView.SelectedIndices[0];
            var item = modListView.SelectedItems[0];
            modListView.Items.RemoveAt(modListView.SelectedIndices[0]);
            int newindex = Math.Max(0, index - 1);
            modListView.Items.Insert(newindex, item);
            modListView.Items[newindex].EnsureVisible();
        }

        private void buttonDown_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            int index = modListView.SelectedIndices[0];
            var item = modListView.SelectedItems[0];
            modListView.Items.RemoveAt(modListView.SelectedIndices[0]);
            int newindex = Math.Min(modListView.Items.Count, index + 1);
            modListView.Items.Insert(newindex, item);
            modListView.Items[newindex].EnsureVisible();
        }

        private void buttonBottom_Click(object sender, EventArgs e)
        {
            if (modListView.SelectedIndices.Count < 1 || modListView.SelectedIndices[0] == -1)
                return;
            var item = modListView.SelectedItems[0];
            modListView.Items.RemoveAt(modListView.SelectedIndices[0]);
            modListView.Items.Add(item);
            modListView.Items[modListView.Items.Count - 1].EnsureVisible();
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            if (building)
            {
                LogMessage("Cancelling build...");
                buildCancel = true;
                cancellationTokenSource.Cancel();
            }
            TerminateEmu();
        }
    }
}