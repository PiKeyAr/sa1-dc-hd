﻿using IniParser.Model;
using IniParser;
using System;
using System.IO;
using System.Linq;

namespace GUIPatcher
{
    public class Mod
    {
        public string Name;
        public string Author;
        public string Version;
        public string Description;
        public string ModPath;
        public string[] FileReplacements;
        public string[] RequiredCodes;
        public bool Autodemo;
        public IniData PatchData;
        public string[] PatternFiles;
        public string[] PatternData;
        public string[] IncludeFolders;

        public Mod(string inifile)
        {
            var ini = new FileIniDataParser();
            PatchData = ini.ReadFile(inifile);
            Name = PatchData.Global["Name"];
            Author = PatchData.Global["Author"];
            Version = PatchData.Global["Version"];
            Description = PatchData.Global["Description"];
            bool res = bool.TryParse(PatchData.Global["Autodemo"], out Autodemo);
            if (!res)
                Autodemo = false;
            ModPath = Path.GetFullPath(Path.GetDirectoryName(inifile));
            var files = Directory.GetFiles(ModPath, "*.*", SearchOption.TopDirectoryOnly).Where(name => !name.EndsWith(".ini", StringComparison.OrdinalIgnoreCase));
            FileReplacements = files.ToArray();
            if (PatchData.Global["RequiredCodes"] != null)
                RequiredCodes = PatchData.Global["RequiredCodes"].Split(',');
            if (PatchData.Global["IncludeFolders"] != null)
                IncludeFolders = PatchData.Global["IncludeFolders"].Split(',');
            if (PatchData.Global["PatternFiles"] != null)
            {
                PatternFiles = PatchData.Global["PatternFiles"].Split(',');
                string patternfile = Path.Combine(Path.GetDirectoryName(inifile), "pattern.ini");
                if (File.Exists(patternfile))
                    PatternData = File.ReadAllLines(patternfile);
            }
        }
    }
}